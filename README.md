## Shipping Service API

UBER-like service for freight trucks, implemented with NodeJS, Express.js in REST style, using MongoDB as a database and Mongoose ODM. 

# User actions

GET /api/users/me <br/>
DELETE /api/users/me <br/>
PATCH /api/users/password

# Driver actions

GET /api/trucks/ <br/>
POST /api/trucks/ <br/>
GET /api/trucks/{id} <br/>
PUT /api/trucks/{id} <br/>
DELETE /api/trucks/{id} <br/>
POST /api/trucks/{id}/assign

# Shipper actions

GET /api/loads/ <br/>
POST /api/loads/ <br/>
GET /api/loads/active <br/>
PATCH /api/loads/active/state <br/>
GET /api/loads/{id} <br/>
PUT /api/loads/{id} <br/>
DELETE /api/loads/{id} <br/>
POST /api/loads/{id}/post <br/>
GET /api/loads/{id}/shipping_info

# Autn actions

POST /api/auth/register <br/>
POST /api/auth/login

# Available scripts

npm install - for installing dependencies

npm start - to start a project
 