const DRIVER = 'DRIVER';
const SHIPPER = 'SHIPPER';

const loadStatus = {
	NEW: 'NEW',
	POSTED: 'POSTED',
	ASSIGNED: 'ASSIGNED',
	SHIPPED: 'SHIPPED',
};

const loadStates = [
	'En route to Pick Up',
	'Arrived to Pick Up',
	'En route to delivery',
	'Arrived to delivery',
];

const TRUCK_TYPES = {
	'SPRINTER': {
		payload: 1700,
		width: 300,
		length: 250,
		height: 170,
	},
	'SMALL STRAIGHT': {
		payload: 2500,
		width: 500,
		length: 250,
		height: 170,
	},
	'LARGE STRAIGHT': {
		payload: 4000,
		width: 700,
		length: 350,
		height: 200,
	},
};

module.exports = {
	DRIVER,
	SHIPPER,
	loadStatus,
	loadStates,
	TRUCK_TYPES,
};
