const { TRUCK_TYPES } = require('./constants');

function defineTruckType({ payload, dimensions }) {
	for (const type of Object.keys(TRUCK_TYPES)) {
		const truckStats = TRUCK_TYPES[type];
		if (
			truckStats.payload >= Number(payload) &&
			truckStats.width >= Number(dimensions.width) &&
			truckStats.length >= Number(dimensions.length) &&
			truckStats.height >= Number(dimensions.height)
		) {
			return type;
		}
	}
}

module.exports = {
	defineTruckType,
};
