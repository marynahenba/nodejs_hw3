const express = require('express');
const router = express.Router();
const {
	registerUser,
	loginUser,
	forgotPassword,
} = require('../services/authService.js');

router.post('/register', async (req, res, next) => {
	try {
		const { role, email, password } = req.body;

		const { user, message } = await registerUser({ role, email, password });

		if (message) {
			return res.status(400).send({ message });
		}

		res.json({ message: 'Profile created successfully' });
	} catch (err) {
		next(err);
	}
});

router.post('/login', async (req, res, next) => {
	try {
		const { email, password } = req.body;

		const jwt_token = await loginUser({ email, password });

		res.json(jwt_token);
	} catch (err) {
		next(err);
	}
});

router.post('/forgot_password', forgotPassword);

module.exports = {
	authRouter: router,
};
