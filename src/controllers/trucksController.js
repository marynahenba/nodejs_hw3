const express = require('express');
const router = express.Router();
const { truckJoiSchema } = require('../models/Trucks');
const {
	getTrucks,
	addTruck,
	getTruckById,
	updateTruckById,
	deleteTruckById,
	assignTruckToUserById,
} = require('../services/trucksService.js');

router.get('/', async (req, res, next) => {
	try {
		const userId = req.user._id;
		const trucks = await getTrucks(userId);

		res.json({ trucks });
	} catch (err) {
		next(err);
	}
});

router.post('/', async (req, res, next) => {
	try {
		const userId = req.user._id;
		const { type } = req.body;

		await truckJoiSchema.validateAsync({ type });

		if (!type) {
			return res.status(400).send({ message: 'Parameter "type" is reqiured' });
		}

		await addTruck(type, userId);
		res.json({ message: 'Truck created successfully', success: true });
	} catch (err) {
		next(err);
	}
});

router.get('/:id', async (req, res, next) => {
	try {
		const truckId = req.params.id;
		const userId = req.user._id;

		const truck = await getTruckById(truckId, userId);

		if (!truck) {
			return res
				.status(400)
				.send({ message: `Truck with id ${req.params.id} is not found` });
		}

		res.json({ truck });
	} catch (err) {
		next(err);
	}
});

router.put('/:id', async (req, res, next) => {
	try {
		const userId = req.user._id;
		const truckId = req.params.id;
		const { type } = req.body;

		if (
			type !== 'SPRINTER' &&
			type !== 'SMALL STRAIGHT' &&
			type !== 'LARGE STRAIGHT'
		) {
			return res
				.status(400)
				.send({ message: 'Please, specify correct type papameter' });
		}

		const updatedTruck = await updateTruckById(truckId, userId, type);

		if (!updatedTruck)
			return res.status(400).send({ message: 'Truck is not found' });

		res.json({ message: 'Truck details changed successfully' });
	} catch (err) {
		next(err);
	}
});

router.delete('/:id', async (req, res, next) => {
	try {
		const userId = req.user._id;
		const truckId = req.params.id;

		const truck = await deleteTruckById(truckId, userId);

		if (!truck) return res.status(400).send({ message: 'Truck is not found' });

		res.json({ message: 'Truck deleted successfully' });
	} catch (err) {
		next(err);
	}
});

router.post('/:id/assign', async (req, res, next) => {
	try {
		const userId = req.user._id;
		const truckId = req.params.id;

		const { message } = await assignTruckToUserById(truckId, userId);

		if (message) {
			return res.status(400).send({ message });
		}

		res.json({ message: 'Truck assigned successfully' });
	} catch (err) {
		next(err);
	}
});

module.exports = {
	trucksRouter: router,
};
