const express = require('express');
const router = express.Router();
const { loadJoiSchema } = require('../models/Loads');
const {
	getLoads,
	addLoad,
	getActiveLoad,
	changeNextLoadState,
	getLoadById,
	updateLoadById,
	deleteLoadById,
	postLoadById,
	getLoadShippingInfoById,
} = require('../services/loadsService');
const {
	driverMiddleware,
	shipperMiddleware,
} = require('../middleware/roleMiddleware');

router.get('/', async (req, res, next) => {
	try {
		const userId = req.user._id;
		const role = req.user.role;
		const loads = await getLoads(userId, role);

		res.json({ loads });
	} catch (err) {
		next(err);
	}
});

router.post('/', shipperMiddleware, async (req, res, next) => {
	try {
		const userId = req.user._id;
		await loadJoiSchema.validateAsync({
			...req.body,
		});
		if (!req.body) {
			return res
				.status(400)
				.send({ message: 'Please, specify correct parameters' });
		}

		await addLoad(req.body, userId);
		return res.json({ message: 'Load created successfully' });
	} catch (err) {
		next(err);
	}
});

router.get('/active', driverMiddleware, async (req, res, next) => {
	try {
		const userId = req.user._id;
		const load = await getActiveLoad(userId);

		if (!load) {
			return res.status(400).send({ message: `Load is not found` });
		}

		res.json({ load });
	} catch (err) {
		next(err);
	}
});

router.patch('/active/state', driverMiddleware, async (req, res, next) => {
	try {
		const userId = req.user._id;
		const { newState, message } = await changeNextLoadState(userId);

		if (message) {
			return res.status(400).send({ message });
		}

		res.json({ message: `Load state changed to ${newState}` });
	} catch (err) {
		next(err);
	}
});

router.get('/:id', shipperMiddleware, async (req, res, next) => {
	try {
		const loadId = req.params.id;
		const userId = req.user._id;

		const load = await getLoadById(loadId, userId);

		if (!load) {
			return res
				.status(400)
				.send({ message: `Load with id ${loadId} is not found` });
		}

		res.json({ load });
	} catch (err) {
		next(err);
	}
});

router.put('/:id', shipperMiddleware, async (req, res, next) => {
	try {
		const userId = req.user._id;
		const loadId = req.params.id;
		const { payload } = req.body;

		if (!payload) {
			return res.status(400).send({ message: 'Please, specify papameters' });
		}

		const { updatedLoad, message } = await updateLoadById(
			loadId,
			userId,
			payload
		);

		if (message) {
			return res.status(400).send({ message });
		}

		res.json({ message: 'Load details changed successfully' });
	} catch (err) {
		next(err);
	}
});

router.delete('/:id', shipperMiddleware, async (req, res, next) => {
	try {
		const userId = req.user._id;
		const loadId = req.params.id;

		const load = await deleteLoadById(loadId, userId);

		if (!load)
			return res
				.status(400)
				.send({
					message: 'Load is not found or you can`t delete active loads',
				});

		res.json({ message: 'Load deleted successfully' });
	} catch (err) {
		next(err);
	}
});

router.post('/:id/post', shipperMiddleware, async (req, res, next) => {
	try {
		const userId = req.user._id;
		const loadId = req.params.id;

		const postedLoad = await postLoadById(loadId, userId);

		res.json(postedLoad);
	} catch (err) {
		next(err);
	}
});

router.get('/:id/shipping_info', shipperMiddleware, async (req, res, next) => {
	try {
		const loadId = req.params.id;
		const userId = req.user._id;

		const { load, truck, message } = await getLoadShippingInfoById(
			loadId,
			userId
		);

		if (message) {
			return res.status(400).send({ message });
		}

		res.json({ load, truck });
	} catch (err) {
		next(err);
	}
});

module.exports = {
	loadsRouter: router,
};
