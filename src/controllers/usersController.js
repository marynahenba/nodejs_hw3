const express = require('express');
const router = express.Router();
const {
	getUser,
	deleteUser,
	changeUserPassword,
} = require('../services/usersService.js');

router.get('/', async (req, res, next) => {
	try {
		const userId = req.user._id;
		const user = await getUser(userId);

		if (!user) {
			return res.status(400).send({ message: 'Something went wrong' });
		}
		res.json({ user });
	} catch (err) {
		next(err);
	}
});

router.delete('/', async (req, res, next) => {
	try {
		const userId = req.user._id;

		const user = await deleteUser(userId);

		if (!user) {
			return res.status(400).send({ message: 'User is not found' });
		}

		res.json({ message: 'Success' });
	} catch (err) {
		next(err);
	}
});

router.patch('/password', async (req, res, next) => {
	try {
		const userId = req.user._id;
		const { oldPassword, newPassword } = req.body;

		const { user, message } = await changeUserPassword(
			userId,
			oldPassword,
			newPassword
		);

		if (message) {
			return res.status(400).send({ message });
		}

		res.json({ message: 'Password changed successfully' });
	} catch (err) {
		next(err);
	}
});

module.exports = {
	usersRouter: router,
};
