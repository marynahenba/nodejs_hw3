const shipperMiddleware = (req, res, next) => {
	return req.user.role === 'SHIPPER'
		? next()
		: res.status(400).json({ message: 'This action is forbidden for drivers' });
};

const driverMiddleware = (req, res, next) => {
	return req.user.role === 'DRIVER'
		? next()
		: res
				.status(400)
				.json({ message: 'This action is forbidden for shippers' });
};

module.exports = {
	shipperMiddleware,
	driverMiddleware,
};
