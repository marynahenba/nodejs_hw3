const { Truck, truckJoiSchema } = require('../models/Trucks.js');

const addTruck = async (type, userId) => {
	const truck = new Truck({
		created_by: userId,
		type,
	});
	await truck.save();
};

const getTrucks = async (userId) => {
	const trucks = await Truck.find({ created_by: userId }, { __v: false });

	if (trucks.length === 0) {
		return { message: 'Client error' };
	}

	return trucks;
};

const getTruckById = async (truckId, userId) => {
	const truck = await Truck.findById({ _id: truckId, created_by: userId });
	return truck;
};

const updateTruckById = async (truckId, userId, type) => {
	const truck = await Truck.findByIdAndUpdate(
		{ _id: truckId, created_by: userId },
		type
	);
	return truck;
};

const deleteTruckById = async (truckId, userId) => {
	const truck = await Truck.findByIdAndDelete({
		_id: truckId,
		created_by: userId,
	});
	return truck;
};

const assignTruckToUserById = async (truckId, userId) => {
	try {
		const assignedTrucks = await Truck.find({ assigned_to: userId });

		if (assignedTrucks.length > 0) {
			return { message: 'Driver has assigned truck' };
		}

		const truck = await Truck.findByIdAndUpdate(
			{ _id: truckId, created_by: userId },
			{ $set: { assigned_to: userId } }
		);
		return truck;
	} catch (err) {
		next(err);
	}
};

module.exports = {
	getTrucks,
	addTruck,
	getTruckById,
	updateTruckById,
	deleteTruckById,
	assignTruckToUserById,
};
