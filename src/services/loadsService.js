const { Load, loadJoiSchema } = require('../models/Loads.js');
const { Truck } = require('../models/Trucks.js');
const {
	loadStates,
	loadStatus,
	SHIPPER,
	DRIVER,
} = require('../helpers/constants');
const { defineTruckType } = require('../helpers/defineTruckType');

const addLoad = async (body, userId) => {
	const load = new Load({
		created_by: userId,
		...body,
	});
	await load.save();
};

const getShippersLoads = async (userId) => {
	const loads = await Load.find({ created_by: userId }, { __v: false });
	if (loads.length === 0) {
		return { message: 'Client error' };
	}
	return loads;
};

const getDriversLoads = async (userId) => {
	const loads = await Load.find(
		{ assigned_to: userId, status: ['ASSIGNED', 'SHIPPED'] },
		{ __v: false }
	);
	return loads;
};

const getLoads = async (userId, role) => {
	switch (role) {
		case DRIVER:
			return await getDriversLoads(userId);
		case SHIPPER:
			return await getShippersLoads(userId);
	}
};

const getActiveLoad = async (userId) => {
	const load = await Load.findOne({
		assigned_to: userId,
		status: [loadStatus.ASSIGNED, loadStatus.SHIPPED],
	});
	return load;
};

const getLoadById = async (loadId, userId) => {
	const load = await Load.findOne({
		_id: loadId,
		created_by: userId,
	});
	return load;
};

const updateLoadById = async (loadId, userId, payload) => {
	const updatedLoad = await Load.findOneAndUpdate(
		{
			_id: loadId,
			created_by: userId,
			status: 'NEW',
		},
		payload
	);

	if (!updatedLoad) {
		return { message: 'Load is not found or you can`t update active loads' };
	}

	return updatedLoad;
};

const deleteLoadById = async (loadId, userId) => {
	const load = await Truck.findOneAndDelete({
		_id: loadId,
		created_by: userId,
		status: 'NEW',
	});
	return load;
};

const changeNextLoadState = async (userId) => {
	const load = await Load.findOne({
		assigned_to: userId,
		status: loadStatus.ASSIGNED,
	});
	const truck = await Truck.findOne({
		assigned_to: userId,
		status: 'OL',
	});

	if (!load) {
		return { message: 'Load was not found' };
	}

	const newState = loadStates[loadStates.indexOf(load.state) + 1];

	if (!newState) {
		return { message: `Load already has ${load.state} state` };
	}

	if (newState === loadStates[3]) {
		load.status = loadStatus.SHIPPED;
		truck.assigned_to = undefined;
		truck.status = 'IS';
		load.logs.push({
			message: `Load was successfully delivered`,
		});
	}
	load.state = newState;
	load.logs.push({
		message: `Load is ${newState}`,
	});
	await load.save();
	await truck.save();
	return { newState };
};

const postLoadById = async (loadId, userId) => {
	const load = await getLoadById(loadId, userId);

	const truckType = defineTruckType({
		payload: load.payload,
		dimensions: load.dimensions,
	});

	const truck = await Truck.findOne({
		status: 'IS',
		assigned_to: { $exists: true },
		type: truckType,
	});

	if (!truck) {
		return { message: 'Load posting failed', driver_found: false };
	}

	truck.status = 'OL';
	await truck.save();

	load.status = loadStatus.ASSIGNED;
	load.state = loadStates[0];
	load.assigned_to = truck.assigned_to;
	load.logs.push({
		message: `Load was assigned to driver with id: ${truck.assigned_to}`,
	});
	await load.save();
	return { message: 'Load posted successfully', driver_found: true };
};

const getLoadShippingInfoById = async (loadId, userId) => {
	const load = await Load.findOne({
		_id: loadId,
		created_by: userId,
		status: loadStatus.ASSIGNED,
	});

	if (!load) {
		return { message: 'Load is not found' };
	}

	const truck = await Truck.findOne({ assigned_to: load.assigned_to });

	if (!truck) {
		return { message: 'Truck is not found' };
	}

	return { load, truck };
};

module.exports = {
	getLoads,
	addLoad,
	getActiveLoad,
	changeNextLoadState,
	getLoadById,
	updateLoadById,
	deleteLoadById,
	postLoadById,
	getLoadShippingInfoById,
};
