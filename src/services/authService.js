const { User, userJoiSchema } = require('../models/Users.js');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const secret = 'secret-jwt-key';

const registerUser = async ({ role, email, password }) => {
	await userJoiSchema.validateAsync({ role, email, password });

	if (!email || !password || !role) {
		return { message: 'Please, specify correct email, password and role' };
	}

	const user = new User({
		role,
		email,
		password: await bcryptjs.hash(password, 10),
	});

	await user.save();
	return user;
};

const loginUser = async ({ email, password }) => {
	const user = await User.findOne({ email });

	if (!user) {
		return { message: 'User is not found' };
	}
	if (!(await bcryptjs.compare(String(password), String(user.password)))) {
		return { message: 'Not authorized' };
	}

	const payload = {
		_id: user._id,
		role: user.role,
		email: user.email,
		created_date: user.created_date,
	};
	const jwt_token = jwt.sign(payload, secret);
	return { jwt_token };
};

function forgotPassword() {}

module.exports = {
	registerUser,
	loginUser,
	forgotPassword,
};
