const { User } = require('../models/Users.js');
const bcrypt = require('bcryptjs');

const getUser = async (userId) => {
	const user = await User.findById({ _id: userId });
	return user;
};

const deleteUser = async (userId) => {
	const user = await User.findByIdAndDelete({ _id: userId });
	return user;
};

const changeUserPassword = async (userId, oldPassword, newPassword) => {
	const user = await User.findById({ _id: userId });

	if (user && bcrypt.compare(String(oldPassword), String(user.password))) {
		hachedPasssword = await bcrypt.hash(newPassword, 10);
		user.password = hachedPasssword;
		await user.save();
		return user;
	} else {
		return { message: 'Wrong password' };
	}
};

module.exports = {
	getUser,
	deleteUser,
	changeUserPassword,
};
