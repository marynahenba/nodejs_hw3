const mongoose = require('mongoose');
const Joi = require('joi');

const userJoiSchema = Joi.object({
	role: Joi.string().valid('DRIVER', 'SHIPPER').required(),
	email: Joi.string().email({
		minDomainSegments: 2,
		tlds: { allow: ['com', 'net'] },
	}),
	password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
});

const User = mongoose.model('User', {
	role: {
		type: String,
		enum: ['DRIVER', 'SHIPPER'],
		required: true,
	},
	email: {
		type: String,
		required: true,
		unique: true,
	},
	password: {
		type: String,
		required: true,
	},
	created_date: {
		type: Date,
		default: new Date(),
	},
});

module.exports = {
	User,
	userJoiSchema,
};
