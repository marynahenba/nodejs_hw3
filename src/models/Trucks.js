const mongoose = require('mongoose');
const Joi = require('joi');
const { TRUCK_TYPES } = require('../helpers/constants');

const truckJoiSchema = Joi.object({
	type: Joi.string()
		.valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
		.required(),
	status: Joi.string().valid('IS', 'OL'),
});

const Truck = mongoose.model('Truck', {
	created_by: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
	},
	assigned_to: {
		type: mongoose.Schema.Types.ObjectId,
	},
	type: {
		type: String,
		enum: Object.keys(TRUCK_TYPES),
		required: true,
	},
	status: {
		type: String,
		enum: ['OL', 'IS'],
		default: 'IS',
	},
	created_date: {
		type: Date,
		default: new Date(),
	},
});

module.exports = {
	Truck,
	truckJoiSchema,
};
