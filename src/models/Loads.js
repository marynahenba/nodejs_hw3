const mongoose = require('mongoose');
const Joi = require('joi');
const { loadStatus, loadStates } = require('../helpers/constants');

const loadJoiSchema = Joi.object({
	status: Joi.string().valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'),
	state: Joi.string().valid(
		'En route to Pick Up',
		'Arrived to Pick Up',
		'En route to delivery',
		'Arrived to delivery'
	),
	name: Joi.string().min(1).max(100).required(),
	payload: Joi.number().min(1).max(4000).required(),
	pickup_address: Joi.string().min(10).max(50).required(),
	delivery_address: Joi.string().min(10).max(50).required(),
	dimensions: Joi.object().keys({
		width: Joi.number().integer().min(1).max(700).required(),
		length: Joi.number().integer().min(1).max(350).required(),
		height: Joi.number().integer().min(1).max(200).required(),
	}),
});

const Load = mongoose.model('Load', {
	created_by: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
	},
	assigned_to: {
		type: mongoose.Schema.Types.ObjectId,
	},
	status: {
		type: String,
		enum: Object.keys(loadStatus),
		default: 'NEW',
	},
	state: {
		type: String,
		enum: loadStates,
	},
	name: {
		type: String,
		required: true,
	},
	payload: {
		type: Number,
		required: true,
	},
	pickup_address: {
		type: String,
		required: true,
	},
	delivery_address: {
		type: String,
		required: true,
	},
	dimensions: {
		type: Object,
		width: {
			type: Number,
			required: true,
		},
		lenght: {
			type: Number,
			required: true,
		},
		height: {
			type: Number,
			required: true,
		},
	},
	logs: {
		type: [
			{
				message: String,
				time: {
					type: Date,
					default: Date.now(),
				},
			},
		],
	},
	created_date: {
		type: Date,
		default: new Date(),
	},
});

module.exports = {
	Load,
	loadJoiSchema,
};
