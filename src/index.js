const express = require('express');
const cors = require('cors');
const morgan = require('morgan');

const corsOptions = {
	origin: '*',
	credentials: true,
	optionSuccessStatus: 200,
};
const app = express();
const mongoose = require('mongoose');

mongoose.connect(
	'mongodb+srv://marynas:sMaM8orQbiCj18ol@marynascluster.lwungkp.mongodb.net/delivery?retryWrites=true&w=majority'
);

const { authRouter } = require('./controllers/authController.js');
const { usersRouter } = require('./controllers/usersController.js');
const { loadsRouter } = require('./controllers/loadsController.js');
const { trucksRouter } = require('./controllers/trucksController.js');
const { authMiddleware } = require('./middleware/authMiddleware.js');
const { driverMiddleware } = require('./middleware/roleMiddleware.js');

app.use(cors(corsOptions));
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth/', authRouter);
app.use('/api/users/me', authMiddleware, usersRouter);
app.use('/api/loads/', authMiddleware, loadsRouter);
app.use('/api/trucks/', authMiddleware, driverMiddleware, trucksRouter);

const start = async () => {
	try {
		app.listen(8080);
		console.log('Conected!');
	} catch (err) {
		console.error(`Error on server startup: ${err.message}`);
	}
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
	console.error(err);
	res.status(500).send({ message: 'Server error' });
}
